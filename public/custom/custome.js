function login(url){   
    $("#lableWrong").html("")
    var username = $("#username").val()
    var password = $("#password").val()

    if (username == "") {
        $("#lableWrong").append('<h5 class="text-white"> - Username tidak boleh kosong  </h5>')
    }else{

    }

    if (password == "") {
        $("#lableWrong").append('<h5 class="text-white"> - Password tidak boleh kosong  </h5>')
    }else{

    }

    if (username != "" && password != "") {
        $.ajax({
            type : "POST",
            url : url+'loginProses',
            data : {username : username, password : password},
            beforeSend: function(){
                $("#loading").addClass('lds-roller');
                $("#bg-loaded").addClass('bg-loaded');
            },success: function(res){
                console.log(res)
                if (res == "ya") {
                    
                    $(".m-login__welcome").html("Login Sukses")  
                    window.location = url+'/home' 
                }else{
                    setTimeout(() => {     
                        $("#lableWrong").append('<h5 class="text-white"> - mintalah Administator untuk menambahkan anda <br> - apakah anda lupa password? gunakan fitu lupa password </h5>')                    
                        $("#loading").removeClass('lds-roller');
                        $("#bg-loaded").removeClass('bg-loaded');
                        $(".m-login__welcome").html("Login gagal") 
                        $("#username").val("")
                        $("#password").val("")  
                    }, 1000);
                }
            },
            error: function(err){
                $("#loading").removeClass('lds-roller');
                $("#bg-loaded").removeClass('bg-loaded');
            }
        })
    
    }
}

