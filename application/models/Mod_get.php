<?php

class Mod_get extends CI_Model {
// Supplier
	var $table = 'supplier'; //nama tabel dari database
	var $column_order = array('kode_supplier','nama_supplier','alamat', 'telepon'); //field yang ada di table user
	var $column_search = array('kode_supplier','nama_supplier','alamat', 'telepon'); //field yang diizin untuk pencarian 
	var $order = array('created_date' => 'desc'); // default order 

// Obat
	var $tableObat = 'obat'; //nama tabel dari database
	var $column_orderObat = array('kode_obat','nama_Obat','harga_beli', 'harga_jual', 'satuan'); //field yang ada di table user
	var $column_searchObat = array('kode_obat','nama_Obat','harga_beli', 'harga_jual', 'satuan'); //field yang diizin untuk pencarian 
	var $orderObat = array('obat.created_date' => 'desc'); // default order 


// User
	var $tableUser = 'sys_users'; //nama tabel dari database
	var $column_orderUSer = array('nama_user','username','nama_akses', 'blokir'); //field yang ada di table user
	var $column_searchUser = array('nama_user','username','nama_akses', 'blokir'); //field yang diizin untuk pencarian 
	var $orderUSer = array('sys_users.created_date' => 'desc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_queryObat()
	{
		
		$this->db->from('obat');
		$this->db->join('satuan', 'satuan.kode_satuan = obat.satuan');

		// $this->db->join('satuan', 'obat.satuan = satuan.kode_satuan');

		$i = 0;
	
		foreach ($this->column_searchObat as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_searchObat) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_orderObat[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->orderObat))
		{
			$order = $this->orderObat;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_queryMinimum()
	{
		
		$this->db->from('obat');
		$this->db->where('obat.stok','<=','obat.min_stok');
		$this->db->join('satuan', 'satuan.kode_satuan = obat.satuan');

		// $this->db->join('satuan', 'obat.satuan = satuan.kode_satuan');

		$i = 0;
	
		foreach ($this->column_searchObat as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_searchObat) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_orderObat[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->orderObat))
		{
			$order = $this->orderObat;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_queryUser()
	{
		
		$this->db->from('sys_users');
		$this->db->join('hak_akses', 'sys_users.hak_akses = hak_akses.id_akses');

		// $this->db->join('satuan', 'obat.satuan = satuan.kode_satuan');

		$i = 0;
	
		foreach ($this->column_searchUser as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_searchUser) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_orderUSer[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->orderUSer))
		{
			$order = $this->orderUSer;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}



	function getDataSupplier()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function getDataUser()
	{
		$this->_get_datatables_queryUser();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function countSupplier()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function countAllSupp()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function allSupplier()
	{
		$this->db->select('*');
        $this->db->from('supplier');
        return $this->db->get();
	}

	public function getSupplierEdit($kd){
		$this->db->select('*');
        $this->db->from('supplier');
		$this->db->where('kode_supplier',$kd);
        return $this->db->get();
	}


	public function getAllSupplier(){
		$this->db->select('*');
        $this->db->from('supplier');
        return $this->db->get();
	}

	public function getAllUser(){
		$this->db->select('*');
		$this->db->from('sys_users');
		$this->db->join('hak_akses', 'sys_users.hak_akses = hak_akses.id_akses');
        return $this->db->get();
	}

	public function getUser($username, $password){
		$this->db->select('*');
		$this->db->from('sys_users');
		$this->db->join('hak_akses','sys_users.hak_akses = hak_akses.id_akses');
		$this->db->where('username',$username);
		$this->db->where('password',$password);
		$this->db->where('blokir','On');
        return $this->db->get();
	}

	public function getAllPermission()
	{
		$this->db->select('*');
		$this->db->from('hak_akses');
		// $this->db->where('nama_akses',');
        return $this->db->get();
	}

    public function getKodeSup(){
        $this->db->select('kode_supplier');
        $this->db->from('supplier');
        $this->db->limit('1');
        $this->db->order_by('kode_supplier','desc');
        return $this->db->get();
	}


	function getDataObat()
	{
		$this->_get_datatables_queryObat();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function getDataMinimum()
	{
		$this->_get_datatables_queryMinimum();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function countAllObat()
	{
		$this->db->from($this->tableObat);
		return $this->db->count_all_results();
	}

	public function countAllUser()
	{
		$this->db->from($this->tableUser);
		return $this->db->count_all_results();
	}

	function countUser()
	{
		$this->_get_datatables_queryUser();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function countObat()
	{
		$this->_get_datatables_queryObat();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function getKodeObat(){
        $this->db->select('kode_obat');
        $this->db->from('obat');
        $this->db->limit('1');
        $this->db->order_by('kode_obat','desc');
        return $this->db->get();
	}

	public function getObatEdit($kd){
		$this->db->select('*');
        $this->db->from('obat');
		$this->db->where('kode_obat',$kd);
        return $this->db->get();
	}

	public function getUserEdit($kd){
		$this->db->select('*');
        $this->db->from('sys_users');
		$this->db->where('id_user',$kd);
        return $this->db->get();
	}

	public function getAllObat(){
		$this->db->select('*');
		$this->db->from('obat');
		$this->db->join('satuan', 'satuan.kode_satuan = obat.satuan');
		return $this->db->get();
	}

	public function cekUsername($username){
		$this->db->select('*');
        $this->db->from('sys_users');
		$this->db->where('username',$username);
        return $this->db->get();
	}

	public function getAllDb(){
		$this->db->select('*');
        $this->db->from('sys_database');
        return $this->db->get();
	}

	public function getDbWhere($id){
		$this->db->select('*');
		$this->db->from('sys_database');
		$this->db->where('id',$id);
        return $this->db->get();
	}

	public function getWherPer($id){
		$this->db->select('*');
		$this->db->from('hak_akses');
		$this->db->where('id_akses',$id);
        return $this->db->get();
	}

	public function getsatuan(){
		$this->db->select('*');
		$this->db->from('satuan');
        return $this->db->get();
	}

	public function getWhereSatuan($id)
	{
		$this->db->select('*');
		$this->db->from('satuan');
		$this->db->where('kode_satuan',$id);
        return $this->db->get();	
	}


}
