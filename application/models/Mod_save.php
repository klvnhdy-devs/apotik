<?php

class Mod_save extends CI_Model {

	var $table = 'supplier'; //nama tabel dari database
	var $column_order = array('kode_supplier', 'nama_supplier','alamat','telpon'); //field yang ada di table user
	var $column_search = array('kode_supplier', 'nama_supplier','alamat','telpon'); //field yang diizin untuk pencarian 
	var $order = array('kode_supplier' => 'asc'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function saveSupp($data)
	{
		return $this->db->insert('supplier', $data);		
	}

	public function updateSupp($data,$id){
		$this->db->update('supplier', $data, array('kode_supplier' => $id));
     	return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}

	public function saveObat($data)
	{
		return $this->db->insert('obat', $data);		
	}

	public function updateObat($data,$id){
		$this->db->update('obat', $data, array('kode_obat' => $id));
     	return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}

	public function saveUser($data)
	{
		return $this->db->insert('sys_users', $data);		
	}

	public function updateUser($data,$id){
		$this->db->update('sys_users', $data, array('id_user' => $id));
     	return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}

	public function savePermission($data)
	{
		return $this->db->insert('hak_akses', $data);	
	}

	public function updatePermission($data,$id){
		return $this->db->update('hak_akses', $data, array('id_akses' => $id));
		// return $id;
     	 
	}

	public function savedb($data){
		return $this->db->insert('sys_database', $data);
	}

	public function saveSatuan($data)
	{
		return $this->db->insert('satuan', $data);
	}
	
	public function updateSatuan($data, $id){
		$this->db->update('satuan', $data, array('kode_satuan' => $id));
     	return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
     	 
	}

}
