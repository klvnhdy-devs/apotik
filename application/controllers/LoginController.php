<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

	public function index()
	{
		if(!empty($this->session->userdata('Username'))){
			redirect('/home');	
		}else{

			$this->load->view('login');
		}
	}

	public function login(){
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));

		$get = $this->Mod_get->getUser($username, $password);

		if ($get->num_rows() > 0 ) {
			foreach($get->result() as $valUser){}
			$str = $valUser->menu;
			$str2 = $valUser->subMenu;

			$perm = explode(",",$str);
			$sub = explode(",",$str2);
			$this->session->set_userdata('userid', $valUser->id_user);
			$this->session->set_userdata('Username', $username);
			$this->session->set_userdata('name', $valUser->nama_user);

			$this->session->set_userdata('MenuDashboard', $perm[0]);
			$this->session->set_userdata('MenuMaster', $perm[1]);
			$this->session->set_userdata('MenuTransaksi', $perm[2]);
			$this->session->set_userdata('MenuLaporan', $perm[3]);

			$this->session->set_userdata('Supplier', $sub[0]);
			$this->session->set_userdata('Obat', $sub[1]);
			$this->session->set_userdata('Pengguna', $sub[2]);
			$this->session->set_userdata('Role', $sub[3]);

			$this->session->set_userdata('Pembelian', $sub[4]);
			$this->session->set_userdata('Penjualan', $sub[5]);
			$this->session->set_userdata('LapPembelian', $sub[6]);
			$this->session->set_userdata('LapPenjualan', $sub[7]);

			echo "ya";
		}else{
			echo "tidak";
		}
	}

	public function logout(){
		session_unset();
		session_destroy();
		redirect('/');
	}


}
