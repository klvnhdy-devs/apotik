
		<!-- begin::Quick Nav -->

		<!--begin:: Global Mandatory Vendors -->
		<script src="<?=base_url('public/')?>custom/custome.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/wnumb/wNumb.js" type="text/javascript"></script>

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<script src="<?=base_url('public/')?>vendors/jquery.repeater/src/lib.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/jquery.repeater/src/repeater.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/block-ui/jquery.blockUI.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/select2/dist/js/select2.full.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/handlebars/dist/handlebars.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/nouislider/distribute/nouislider.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/autosize/dist/autosize.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/dropzone/dist/dropzone.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/summernote/dist/summernote.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/markdown/lib/markdown.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/js/framework/components/plugins/forms/jquery-validation.init.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/js/framework/components/plugins/base/bootstrap-notify.init.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/toastr/build/toastr.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/jstree/dist/jstree.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/raphael/raphael.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/morris.js/morris.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/chartist/dist/chartist.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/js/framework/components/plugins/charts/chart.init.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/counterup/jquery.counterup.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
		<script src="<?=base_url('public/')?>vendors/js/framework/components/plugins/base/sweetalert2.init.js" type="text/javascript"></script>

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Bundle -->
		<script src="<?=base_url('public/demos/default/')?>assets/demo/base/scripts.bundle.js" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors -->
		<script src="<?=base_url('public/demos/default/')?>assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts -->
		<script src="<?=base_url('public/demos/default/')?>assets/app/js/dashboard.js" type="text/javascript"></script>

		<!--end::Page Scripts -->

		<script src="<?=base_url('public/custom/')?>datatables.bundle.js" type="text/javascript"></script>
		<script src="<?=base_url('public/custom/')?>treeview.js" type="text/javascript"></script>
		<script src="<?=base_url('public/custom/')?>bootstrap-datepicker.js" type="text/javascript"></script>
		


	</body>

	<!-- end::Body -->
</html>