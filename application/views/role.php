<?php
	$this->load->view('header');

?>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

			<!-- BEGIN: Header -->

			<?php $this->load->view('nav')?>


			<!-- END: Header -->

			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

				<?php $this->load->view('sidebar')?>

                <!-- END: Left Aside -->
                <div class="m-grid__item m-grid__item--fluid m-wrapper">
                    <div class="m-content">
                        <div class="row">
                            <div class="col-lg-12">
    
                                <!--begin::Portlet-->
                                <div class="m-portlet m-portlet--full-height">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">
                                                Role
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="m-portlet__head-tools">
                                        <ul class="m-portlet__nav">
                                            <li class="m-portlet__nav-item">
                                                <a href="#" data-toggle="modal" id="tambah" data-target="#modalRole" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
                                                    <span>
                                                        <i class="la la-plus"></i>
                                                        <span>Tambah Data</span>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                    <div class="m-portlet__body">
    
                                        <!--begin::Section-->
                                        <div class="m-accordion m-accordion--bordered" id="m_accordion_2" role="tablist">
    
                                        <?php 
                                        
                                            foreach ($hakAkses as $valHak) { 
                                                $str = $valHak->menu;
                                                $str2 = $valHak->subMenu;
                                                $perm = explode(",",$str);                                                    
                                                $sub = explode(",",$str2);                                                    
                                            ?>
                                            <div class="m-accordion__item">
                                                <div class="m-accordion__item-head collapsed" role="tab" id="accord<?=$valHak->id_akses?>" data-toggle="collapse" href="#accord<?=$valHak->id_akses?>" aria-expanded="false">
                                                    <span class="m-accordion__item-icon"></span>
                                                    <span class="m-accordion__item-title"><?=$valHak->nama_akses?></span>
                                                    <span class="m-accordion__item-mode"></span>
                                                </div>
                                                <div class="m-accordion__item-body collapse" id="accord<?=$valHak->id_akses?>" role="tabpanel" aria-labelledby="accord<?=$valHak->id_akses?>" data-parent="#m_accordion_2" style="">
                                                    <div class="m-accordion__item-content">
                                                        <div class="row">
                                                            <div class="col-md-6"> 
                                                                <div class="m-portlet__head-action">
                                                                    <?php 
                                                                        $countPerm = count($perm);
                                                                        for ($i=0; $i < $countPerm; $i++) { 
                                                                            if ($perm[$i] != "0") {
                                                                            ?>
                                                                                <button type="button" class="btn btn-sm m-btn--pill  btn-brand">
                                                                                    <?=$perm[$i]?>
                                                                                </button>
                                                                            <?php
                                                                            }       
                                                                        }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6"> 
                                                                <div class="m-portlet__head-action">
                                                                    <?php 
                                                                        $countSub = count($sub);
                                                                        for ($a=0; $a < $countSub; $a++) { 
                                                                            if ($sub[$a] != "0") {
                                                                            ?>
                                                                                <button type="button" class="mb-3 btn btn-sm m-btn--pill  btn-brand">
                                                                                    <?=$sub[$a]?>
                                                                                </button>
                                                                            <?php
                                                                            }       
                                                                        }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-10">
                                                            </div>
                                                            <div class="col-md-2">
                                                                    <button onclick="getEditPer(<?=$valHak->id_akses?>)" data-toggle="modal" data-target="#modalRole" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air">
                                                                        <i class="flaticon-edit"></i> 
                                                                    </button> 
                                                                    <button onclick="deletePer(<?=$valHak->id_akses?>)" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air">
                                                                        <i class="flaticon-delete-1"></i>
                                                                    </button> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php
                                            }
                                        ?>
                                        </div>
    
                                        <!--end::Section-->
                                    </div>
                                </div>
    
                                <!--end::Portlet-->
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>

		<!-- end:: Page -->

<?php
	$this->load->view('modal');
	$this->load->view('footer');
?>

<script>
    var Dashboard;
    var data;
    $(document).ready(function() {
        $('#menuParentMaster').hide();
        $('#menuParentTransaksi').hide();
        $('#menuParentLaporan').hide();
        $('#tambah').click(function(){
            $("#jenisPermission").val("tambah")
            $("#namaRole").val("")
            $("#ParentDashboard").removeAttr('checked','checked')
            $("#ParentDashboard").attr('data-val',0)  
        })

    });                                    
    function cekVal(menu){
        var data = $("#"+menu).attr("data-val")
        if (data == 1) {
            $("#"+menu).attr("data-val", 0)
            $('#menu'+menu).slideUp();
        }else{
            $("#"+menu).attr("data-val", 1)
            $('#menu'+menu).slideDown();
        }
    }
    function setPermission(){
        var valNama = $('#namaRole').val()
        var jenisInput = $('#jenisPermission').val()
        var dataid = $('#idRole').val()
        console.log(dataid)
        // Main Menu
        var Dashboard = $('#ParentDashboard').attr('data-val')
        var Master = $('#ParentMaster').attr('data-val')
        var Transaksi = $('#ParentTransaksi').attr('data-val')
        var Laporan = $('#ParentLaporan').attr('data-val')
        
        // Sub Menu
        // ---------- Master
        var SubSupplier = $('#SubSupplier').attr('data-val')
        var SubObat = $('#SubObat').attr('data-val')
        var SubPengguna = $('#SubPengguna').attr('data-val')
        var SubRole = $('#SubRole').attr('data-val')

        // ---------- Transaksi
        var SubPembelian = $('#SubPembelian').attr('data-val')
        var SubPenjualan = $('#SubPenjualan').attr('data-val')
        
        // ----------- Laporan
        var LapPembelian = $('#LapPembelian').attr('data-val')
        var LapPenjualan = $('#LapPenjualan').attr('data-val')



        if (Dashboard == "0" && Master == "0" && Transaksi == "0" && Laporan == "0") {
            swal({
                type: 'warning',
                title: 'Permission tidak boleh kosong semua',
                showConfirmButton: false,
                timer: 1500
            });
        }else{
            var valSentMaster = 1;
            var valSentTransaksi = 1;
            var valSentLaporan = 1;

            if (Master != "0") {
                // console.log('master tidak kosongx')
                // console.log(SubSupplier)
                if (SubSupplier == "0" && SubObat == "0" && SubPengguna == "0" && SubRole == "0") {
                    swal({
                        type: 'warning',
                        title: 'Sub Master tidak boleh kosong semua',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    valSentLaporan = 0
                }else{
                    valSentLaporan = 1
                }
            }

            if (Transaksi != "0") {
                if (SubPembelian == "0" && SubPenjualan == "0") {
                    swal({ 
                        type: 'warning',
                        title: 'Sub Transaksi tidak boleh kosong semua',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    valSentLaporan = 0
                }else{
                    valSentLaporan = 1
                }
            }

            if (Laporan != "0") {
                if (LapPembelian == "0" && LapPenjualan == "0") {
                    swal({ 
                        type: 'warning',
                        title: 'Sub Laporan tidak boleh kosong semua',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    valSentLaporan = 0
                }else{
                    valSentLaporan = 1
                }
            }


                var valDashboard = ((Dashboard == "1")?'Dashboard':'0')
                var valMaster = ((Master == "1")?'Master':'0')
                var valTransaksi = ((Transaksi == "1")?'Transaksi':'0')
                var valLaporan = ((Laporan == "1")?'Laporan':'0')

                var valSupplier = ((SubSupplier == "1")?'Supplier':'0')
                var valObat = ((SubObat == "1")?'Obat':'0')
                var valPengguna = ((SubPengguna == "1")?'Pengguna':'0')
                var valRole = ((SubRole == "1")?'Role':'0')

                var valPembelian = ((SubPembelian == "1")?'Pembelian':'0')
                var valPenjualan = ((SubPenjualan == "1")?'Penjualan':'0')

                var valLapPembelian = ((LapPembelian == "1")?'LapPembelian':'0')
                var valLapPenjualan = ((LapPenjualan == "1")?'LapPenjualan':'0')


                



                var Menu = valDashboard+","+valMaster+","+valTransaksi+","+valLaporan;
                var SubMenu = valSupplier+","+valObat+","+valPengguna+","+valRole+","+valPembelian+","+valPenjualan+","+valLapPembelian+","+valLapPenjualan
                if (valSentMaster == 1 && valSentTransaksi == 1 && valSentLaporan == 1 ) {
                    $.ajax({
                        type : "POST",
                        url : '<?= base_url('savePermission')?>',
                        data : {jenisInput : jenisInput, dataId:dataid, nama:valNama, menu : Menu, SubMenu : SubMenu},
                        success: function(res){
                            $('#modalRole').modal('toggle')
                            swal({
                                type: 'success',
                                title: 'Role',
                                text : 'Data Role telah di simpan',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            console.log('hasil Res' + res)
                            setTimeout(() => {
                                window.location = '<?=base_url('role')?>';
                            }, 1500);
                        }
                    })              
                }
        }
    }
    function deletePer(id){
        console.log(id)
        swal({
            title: 'Delete data?',
            text: "Data akan di hapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus',
            cancelButtonText: 'tidak',
            reverseButtons: true
        }).then(function(result){
            if (result.value) {
                $.ajax({
                    type : "POST",
                    url : '<?=base_url('delPer')?>',
                    data : {dataId : id},
                    success: function(res){
                        console.log(res)
                        swal({
                            type: 'success',
                            title: 'Data berhasil di hapus',
                            showConfirmButton: false,
                            timer: 1500
                        });			
                        setInterval(() => {
                            window.location = "<?=base_url('role')?>"
                        }, 1500);	
                    }
                })
            } else if (result.dismiss === 'cancel') {
                swal({
                    type: 'info',
                    title: 'Data tidak dihapus',
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        });
    }

    function getEditPer(id){
        console.log(id)
        $("#jenisPermission").val("update")
        $("#idRole").val(id)
        $.ajax({
            type : "POST",
            url : '<?=base_url('getPermission')?>',
            data : {dataId : id},
            success: function(res){
                var obj = JSON.parse(res)
                $("#namaRole").val(obj[0].nama_akses)
                var Menu = obj[0].menu
                var SubMenu = obj[0].subMenu
                var arrayMenu = Menu.split(",");
                var arraySubMenu = SubMenu.split(",");
                if (arrayMenu[0] == "Dashboard") {
                    $("#ParentDashboard").attr('checked','checked')
                    $("#ParentDashboard").attr('data-val',1)   
                }

                if (arrayMenu[1] == "Master") {
                    $("#ParentMaster").attr('checked','checked')
                    $("#ParentMaster").attr('data-val',1)   
                    $('#menuParentMaster').slideDown();
                }

                if (arrayMenu[2] == "Transaksi") {
                    $("#ParentTransaksi").attr('checked','checked')
                    $("#ParentTransaksi").attr('data-val',1)   
                    $('#menuParentTransaksi').slideDown();
                }

                if (arrayMenu[3] == "Laporan") {
                    $("#ParentLaporan").attr('checked','checked')
                    $("#ParentLaporan").attr('data-val',1)   
                    $('#menuParentLaporan').slideDown();
                }

                console.log(arraySubMenu)

                if (arraySubMenu[0] == "Supplier") {
                    $("#SubSupplier").attr('checked','checked')
                    $("#SubSupplier").attr('data-val',1)
                }

                if (arraySubMenu[1] == "Obat") {
                    $("#SubObat").attr('checked','checked')
                    $("#SubObat").attr('data-val',1)
                }

                if (arraySubMenu[2] == "Pengguna") {
                    $("#SubPengguna").attr('checked','checked')
                    $("#SubPengguna").attr('data-val',1)
                }

                if (arraySubMenu[3] == "Role") {
                    $("#SubRole").attr('checked','checked')
                    $("#SubRole").attr('data-val',1)
                }

                if (arraySubMenu[4] == "Pembelian") {
                    $("#SubPembelian").attr('checked','checked')
                    $("#SubPembelian").attr('data-val',1)
                }

                if (arraySubMenu[5] == "Penjualan") {
                    $("#SubPenjualan").attr('checked','checked')
                    $("#SubPenjualan").attr('data-val',1)
                }

                if (arraySubMenu[6] == "LapPembelian") {
                    $("#LapPembelian").attr('checked','checked')
                    $("#LapPembelian").attr('data-val',1)
                }

                if (arraySubMenu[7] == "LapPenjualan") {
                    $("#LapPenjualan").attr('checked','checked')
                    $("#LapPenjualan").attr('data-val',1)
                }


            }
        })
    }


</script>