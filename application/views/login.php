<?php
	$this->load->view('header');

?>
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
			<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
				<div class="m-stack m-stack--hor m-stack--desktop">
					<div class="m-stack__item m-stack__item--fluid">
						<div class="m-login__wrapper">
							<div class="m-login__signin">
								<div class="m-login__head">
									<h3 class="m-login__title">Sign In</h3>
									<hr>
								</div>
								<form class="m-login__form m-form">
									<div class="form-group m-form__group">
										<input class="form-control m-input" type="text" placeholder="Username" name="username" id="username" autocomplete="off">
									</div>
									<div class="form-group m-form__group">
										<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" id="password" name="password">
									</div>
									<div class="row m-login__form-sub">
										<div class="col m--align-left">
											<label class="m-checkbox m-checkbox--focus">
												<input type="checkbox" name="remember"> Remember me
												<span></span>
											</label>
										</div>
										<div class="col m--align-right">
											<a href="javascript:;" id="m_login_forget_password" class="m-link text-black">Forget Password ?</a>
										</div>
									</div>
									<div class="m-login__form-action">
										<button type="button" id="m_login_signin_submit" onclick="login('<?=base_url()?>')"  class="btn btn-focus btn-info m-btn m-btn--pill m-btn--custom m-btn--air">Sign In</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>			
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content m-grid-item--center" style="background:#009ee6">
				<div class="m-grid__item">
					<h3 class="m-login__welcome">Selamat Datang</h3>
					<hr class="bg bg-white">
					<div id="lableWrong">
							
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="" id="loading">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>

	<div id="bg-loaded">

	</div>

<?php
	$this->load->view('footer');
?>
