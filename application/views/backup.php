<?php
	$this->load->view('header');

?>
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<?php $this->load->view('nav')?>
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<?php $this->load->view('sidebar')?>
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<div class="m-content">
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
										</h3>
									</div>
								</div>
								<div class="m-portlet__head-tools">
									<ul class="m-portlet__nav">
										<li class="m-portlet__nav-item">
											<a href="<?=base_url('prosesBackup')?>" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
												<span>
													<i class="fa fa-file-export"></i>
													<span>Backup Database</span>
												</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="m-portlet__body">
								<table id="db" class="table table-striped- table-bordered table-hover table-checkable">
									<thead>
										<tr>
											<th>No</th>
											<th>Nama File</th>
											<th>Tanggal Backup</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no = 1;
											foreach ($listDb as $val) {
												
										?>
											<tr>
												<td><?=$no?></td>
												<td><?=$val->nama_file?></td>
												<td><?=$val->created_date?></td>
												<td>
												<center>
													<a href="<?=base_url('GetController/getDb/')?>/<?=$val->id?>" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air">
														<i class="fa fa-cloud-download-alt"></i>
													</a>
													<button onclick="deleteDb(<?=$val->id?>)" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air">
														<i class="flaticon-delete-1"></i>
													</button>
												</center>
												</td>
											</tr>
										<?php
											$no ++;
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php
	$this->load->view('modal');
	$this->load->view('footer');
?>
<script type="text/javascript">
    var table;
    $(document).ready(function() {
        table = $('#db').DataTable();
	 });

	 function getDb(id) {
		$.ajax({
			type : "POST",
			url : '<?=base_url('getDb')?>',
			data : {dataId : id},
			success: function(res){
				console.log(res)
			}
		})
	 }

	 function deleteDb(id){
		 console.log(id)
		swal({
            title: 'Delete data?',
            text: "Data akan di hapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus',
            cancelButtonText: 'tidak',
            reverseButtons: true
        }).then(function(result){
            if (result.value) {
               $.ajax({
					type : "POST",
					url : '<?=base_url('delDb')?>',
					data : {dataId : id},
					success: function(res){
						console.log(res)
						swal({
							type: 'success',
							title: 'Data berhasil di hapus',
							showConfirmButton: false,
							timer: 1500
						});				
						refreshDataTable(res)
					}
				})
            } else if (result.dismiss === 'cancel') {
                swal({
                    type: 'info',
                    title: 'Data tidak dihapus',
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        });
	 }

	 function refreshDataTable(res){
		var obj = JSON.parse(res);
		var dataDb = [];
		no = 1;
			for (let index = 0; index < obj.length; index++) {
				var arraynew = [no, obj[index].nama_file, obj[index].created_date,'<center><a href="<?=base_url('GetController/getDb/')?>/'+obj[index].id+'" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"><i class="fa fa-cloud-download-alt"></i></a> <button onclick=deleteDb('+obj[index].id+') class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"><i class="flaticon-delete-1"></i></button></center>'  ];
				dataDb.push(arraynew);   
				no++;
			}
		$('#db').DataTable( {
			destroy: true,
			data: dataDb,
			columns: [
				{ title: "No" },
				{ title: "Nama File" },
				{ title: "Tanggal Backup" },
				{ title: "Aksi" },
			]
		} );
	}

</script>
