<?php
	$this->load->view('header');

?>
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<?php $this->load->view('nav')?>
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<?php $this->load->view('sidebar')?>
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<div class="m-content">
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
										</h3>
									</div>
								</div>
								<div class="m-portlet__head-tools">
									<ul class="m-portlet__nav">
										<li class="m-portlet__nav-item">
											<a href="#" data-toggle="modal" id="satuan" data-role="0" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
												<span>
													<span id="lbl">Show Data Satuan</span>
												</span>
											</a>
										</li>
										<li class="m-portlet__nav-item">
											<a href="#" data-toggle="modal" id="tambah" data-target="#modal_obat" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
												<span>
													<i class="la la-plus"></i>
													<span>Tambah Data</span>
												</span>
											</a>
										</li>
										<li class="m-portlet__nav-item">
											<a href="<?=base_url('exportObat')?>" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
												<span>
													<i class="fa fa-file-export"></i>
													<span>Export</span>
												</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
							<div id="datasatuan">
								<div class="m-portlet__body">
									<button href="#" data-toggle="modal" id="tambahSatuan" data-target="#modal_satuan"> Tambah satuan </button>
									<ul class="m-portlet__nav">
										<li class="m-portlet__nav-item">
											<table id="tbSatuan" class="table table-striped- table-bordered table-hover table-checkable">
										</li>
									</ul>
										<thead>
											<tr>
												<th>Satuan</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>

							<div class="m-portlet__body">
								<table id="Obat" class="table table-striped- table-bordered table-hover table-checkable">
									<thead>
										<tr>
											<th>Kode Obat</th>
											<th>Nama Obat</th>
											<th>Harga Beli</th>
											<th>Harga Jual</th>
											<th>Satuan</th>
											<th>Min Stok</th>
											<th>Stok</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php
	$this->load->view('modal');
	$this->load->view('footer');
?>
<script type="text/javascript">
    var table;
    $(document).ready(function() {
		$("#datasatuan").hide()
        table = $('#Obat').DataTable({ 

            "processing": true, 
            "serverSide": true, 
            "order": [], 
            
            "ajax": {
                "url": '<?=base_url('getObat')?>',
                "type": "POST"
            },

            
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],

		});

		$("#satuan").click(function(){
			var role = $(this).attr('data-role')
			if (role == 1) {
				$("#lbl").html("Show Data Satuan")
				$(this).attr('data-role', "0")
				$("#datasatuan").slideUp()
			}else{
				$("#lbl").html("Hide Data Satuan")
				$(this).attr('data-role', "1")
				$("#datasatuan").slideDown()
				$.ajax({
					url : '<?=base_url('getSatuan')?>',
					success: function(res){
						refreshDataSatuan(res)
					}
				})
			}


		})

		$("#tambah").click(function(){
			$("#namaObat").val("")
			$("#minstok").val("")
			$("#hargaBeli").val("")
			$("#hargaJual").val("")
			$("#satuan").val("")
    		$.ajax({
				url : '<?=base_url('kodeObat')?>',
				success: function(res){
					var dataKode;
					var newKode = res.toString();
					if(newKode.length == 1){
						dataKode = 'OB000'+newKode;
					}else if(newKode.length == 2){
						dataKode = 'OB00'+newKode;
					}else if(newKode.length == 3){
						dataKode = 'OB0'+newKode;
					}else{
						dataKode = 'OB'+newKode;
					}
					console.log(dataKode)
					$('#kdObat').val(dataKode)
					$("#jenisObat").val("tambah")
					$("#title").html("Tambah Data Supplier")
				}
			})
  		});

		$("#tambahSatuan").click(function(){
			$("#typeSatuan").val("tambah")
		})

    });


	function updateObat(){
		$.ajax({
			type : "POST",
			url : '<?=base_url('saveObat')?>',
			data: $("#editObatForm").serialize(),
			success: function(res){
				$('#modal_obat').modal('toggle')
				
				swal({
					type: 'success',
					title: 'Data berhasil di simpan',
					showConfirmButton: false,
					timer: 1500
				});
				refreshDataTable(res)
			}
		})
	}

	function getObatDetail(kd){
		$.ajax({
			type : "POST",
			url : '<?=base_url('getObatEdit')?>',
			data : {dataId : kd},
			success: function(res){
				console.log(res)
				var obj = JSON.parse(res);
				console.log(obj)
				$("#kdObat").val(obj.kode)
				$("#namaObat").val(obj.nama)
				$("#minstok").val(obj.minstok)
				$("#hargaBeli").val(obj.hargabeli)
				$("#hargaJual").val(obj.hargajual)
				$("#satuan").val(obj.satuan)
				$("#jenisObat").val("update")
				$("#titleObat").html("Edit Data Obat")
			}
		})
	}


	function getObatDelete(kd){
		swal({
            title: 'Delete data?',
            text: "Data akan di hapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus',
            cancelButtonText: 'tidak',
            reverseButtons: true
        }).then(function(result){
            if (result.value) {
               $.ajax({
					type : "POST",
					url : '<?=base_url('delObat')?>',
					data : {dataId : kd},
					success: function(res){
						swal({
							type: 'success',
							title: 'Data berhasil di hapus',
							showConfirmButton: false,
							timer: 1500
						});				
						refreshDataTable(res)
					}
				})
            } else if (result.dismiss === 'cancel') {
                swal({
                    type: 'info',
                    title: 'Data tidak dihapus',
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        });
	}

	function getKodeEdit(kode){
		console.log("akses get kode edit")
		var dataKode;
		var newKode = kode.toString();
		if(newKode.length == 1){
			dataKode = 'OB000'+newKode;
		}else if(newKode.length == 2){
			dataKode = 'OB00'+newKode;
		}else if(newKode.length == 3){
			dataKode = 'OB0'+newKode;
		}else{
			dataKode = 'OB'+newKode;
		}

		getObatDetail(dataKode)
	}

	function getKodeDelete(kode){
		console.log("akses get kode delete")
		var dataKode;
		var newKode = kode.toString();
		if(newKode.length == 1){
			dataKode = 'OB000'+newKode;
		}else if(newKode.length == 2){
			dataKode = 'OB00'+newKode;
		}else if(newKode.length == 3){
			dataKode = 'OB0'+newKode;
		}else{
			dataKode = 'OB'+newKode;
		}

		getObatDelete(dataKode)
	}


	function refreshDataTable(res){
		var obj = JSON.parse(res);
		var dataObat = [];
			for (let index = 0; index < obj.length; index++) {
				var kode = obj[index].kode_obat;
				var kd_obat = parseInt(kode.replace('OB',''))
				var arraynew = [obj[index].kode_obat,obj[index].nama_obat,obj[index].harga_beli,obj[index].harga_jual,obj[index].nama_satuan,obj[index].min_stok,obj[index].stok,'<center><button onclick="getKodeEdit('+kd_obat+')" data-toggle="modal" data-target="#modal_obat" kode="'+obj[index].kode_obat+'" id="btnGetSupp'+obj[index].kode_obat+'" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"><i class="flaticon-edit"></i> </button> <button kode="'+obj[index].kode_obat+'" onclick="getKodeDelete('+kd_obat+')" id="btnDelete'+obj[index].kode_obat+'" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"><i class="flaticon-delete-1"></i> </button> </center>',];
				dataObat.push(arraynew);   
			}
		$('#Obat').DataTable( {
			destroy: true,
			data: dataObat,
			columns: [
				{ title: "Kode Obat" },
				{ title: "Nama Obat" },
				{ title: "Harga Beli" },
				{ title: "Harga Jual" },
				{ title: "Satuan" },
				{ title: "Min Stok" },
				{ title: "Stok" },
				{ title: "Aksi" }
			]
		} );
	}

	function refreshDataSatuan(res){
		var obj = JSON.parse(res);
		var dataObat = [];
			for (let index = 0; index < obj.length; index++) {
				var arraynew = [obj[index].nama_satuan,'<center><button onclick="EditSatuan('+obj[index].kode_satuan+')" data-toggle="modal" data-target="#modal_satuan"  class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"><i class="flaticon-edit"></i> </button> <button onclick="getDeleteSatuan('+obj[index].kode_satuan+')" id="btnDelete'+obj[index].kode_obat+'" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"><i class="flaticon-delete-1"></i> </button> </center>',];
				dataObat.push(arraynew);   
			}
		$('#tbSatuan').DataTable( {
			destroy: true,
			data: dataObat,
			columns: [
				{ title: "Satuan" },
				{ title: "Aksi" }
			]
		} );
	}

	function getDeleteSatuan(id) {
		swal({
            title: 'Delete data?',
            text: "Data akan di hapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus',
            cancelButtonText: 'tidak',
            reverseButtons: true
        }).then(function(result){
            if (result.value) {
				$.ajax({
					type : "POST",
					url : '<?=base_url('getDeleteSatuan')?>',
					data : {dataId : id},
					success: function(res){
						swal({
							type: 'success',
							title: 'Data berhasil di simpan',
							showConfirmButton: false,
							timer: 1500
						});
						refreshDataSatuan(res)
					}
				})
			} else if (result.dismiss === 'cancel') {
                swal({
                    type: 'info',
                    title: 'Data tidak dihapus',
                    showConfirmButton: false,
                    timer: 1500
                });
			}
		});
	}

	function EditSatuan(id) {
		var input = $("#idSatuan").val(id)
		var type = $("#typeSatuan").val("update") 
		$.ajax({
			type : "POST",
			url : '<?=base_url('getEditSatuan')?>',
			data : {dataId : id},
			success: function(res){
				var obj = JSON.parse(res)
				$("#inputSatuan").val(obj[0].nama_satuan)
			}
		})
		
	}

	function btnSaveSatuan() {
		var input = $("#inputSatuan").val()
		var type = $("#typeSatuan").val() 
		var id = $("#idSatuan").val()
		$.ajax({
			type : "POST",
			url : '<?=base_url('saveSatuan')?>',
			data : {satuan : input,type:type,dataId:id},
			success: function(res){
				console.log(res)
				$('#modal_satuan').modal('toggle')
				swal({
					type: 'success',
					title: 'Data berhasil di simpan',
					showConfirmButton: false,
					timer: 1500
				});
				refreshDataSatuan(res)
			}
		})
	}

</script>
