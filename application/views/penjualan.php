<?php
include 'header.php';
?>

	<body class="m-content--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside--offcanvas-default">
		<div class="m-grid m-grid--hor m-grid--root m-page">

			<!-- BEGIN: Header -->
            <?php
                include 'nav.php';
            ?>

			<!-- END: Header -->

            <!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body" style="padding-top: 180px!important;" >

				<!-- BEGIN: Left Aside -->
                <?php
                    include 'sidebar.php';
                ?>

				<!-- END: Left Aside -->
				<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- END: Subheader -->
					<div class="m-content">

						<!--Begin::Section-->
						<div class="row">
							<div class="col-xl-12">

								<!--begin:: Widgets/New Users-->
								<div class="m-portlet m-portlet--full-height ">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item">
													<a href="#" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
														<span>
															<i class="la la-plus"></i>
															<span>Tambah Data</span>
														</span>
													</a>
												</li>
												<li class="m-portlet__nav-item">
													<a href="#" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
														<span>
															<i class="fa fa-file-export"></i>
															<span>Export</span>
														</span>
													</a>
												</li>
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">
										<table class="table table-striped- table-bordered table-hover table-checkable" id="tablePenjualan">
											<thead>
												<tr>
													<th>No</th>
													<th>No Penjualan</th>
													<th>Tanggal</th>
													<th>Total</th>
													<th>Aksi</th>
												</tr>
											</thead>
										</table>
									</div>
								</div>

								<!--end:: Widgets/New Users-->
							</div>
						</div>

						<!--End::Section-->
					</div>
				</div>
			</div>

			<!-- end:: Body -->
		</div>
<?php 
include 'footer.php';
?>